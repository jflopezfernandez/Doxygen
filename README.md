
# Doxygen

I don't think it would be an exaggeration to call Doxygen one of the best 
libraries in the C++ sphere, based on both usefulness and popularity, so I've
always been confused be the apparent "ghost-town" feeling I get when I take a 
leisurely stroll through the project directories. The programming style is a 
combination of C89 and C++14, with leaky abstractions and inconsistent code 
style throughout.

This fork is my swan song to the Doxygen project, where I plan on giving this
code base the code it deserves. Ironically, a lot of the code lacks 
documentation, at least as I would perceive it. I can't stand mediocre coding 
habits and imperfect or inelegant solutions, so I decided to let everyone 
continue to live their life in peace as I work on a completely separate version.

For the moment I have no plans to add any functionality as the original 
application already does a pretty outstanding job in my opinion; this really is
a cosmetic endeavor for the time being, for the sake of code maintenance and 
discipline.

# Building

CMake is a pretty robust buildchain generator, so everything should be working
exactly as before.

### Linux

```
cmake -G "Unix Makefiles" .
cmake --build . --config Release
```

### Windows

At the time of this writing Visual Studio 19 has been released, but CMake does
not yet support using it as a generator. You may thus continue to build the 
project as you presumably would have for the past two years.

```
cmake -G "Visual Studio 15 Win64" -T host=x64 .
cmake --build . --config Release
```
